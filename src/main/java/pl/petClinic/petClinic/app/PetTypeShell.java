package pl.petClinic.petClinic.app;

import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import pl.petClinic.petClinic.model.PetType;
import pl.petClinic.petClinic.repo.repo.PetTypeRepository;

import java.util.Collection;
import java.util.List;

@ShellComponent
public class PetTypeShell {

    PetTypeRepository petTypeRepository = new PetTypeRepository();

    @ShellMethod("Creates a new pet type")
    public void petTypeCreate(String name){
        PetType petType = new PetType();
        petType.setName(name);
        petTypeRepository.create(petType);

    }

    @ShellMethod("Lists all pet types")
    public Collection<PetType> petTypeList(){
        return petTypeRepository.getAll();
    }

    @ShellMethod("Updates a specific pet type")
    public void petTypeUpdate(int id, String newName){
        petTypeRepository.get(id).setName(newName);
    }

}

