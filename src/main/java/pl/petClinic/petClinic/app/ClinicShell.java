package pl.petClinic.petClinic.app;

import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

@ShellComponent
public class ClinicShell {

    @ShellMethod("help")
    public String welcome(){
        return "Witamy w naszej klinice v0.01";
    }

}
