package pl.petClinic.petClinic.repo.repo;

import pl.petClinic.petClinic.model.PetType;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class PetTypeRepository {

    private Map<Integer, PetType> items = new HashMap<>();
    private int id;

    public void create(PetType petType){
        petType.setId(id++);
        items.put(petType.getId(), petType);
    }

    public PetType get(int id){
        return items.get(id);
    }

    public Collection<PetType> getAll(){
        return items.values();
    }

    public void update(PetType petType){

    }

}
